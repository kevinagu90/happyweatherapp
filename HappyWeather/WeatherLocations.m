//
//  WeatherLocations.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 7/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "WeatherLocations.h"

@interface WeatherLocations ()
@property (nonatomic, retain) NSMutableArray<Weather*> *weathers;
@end
@implementation WeatherLocations

+ (instancetype)sharedWeathers {
    static WeatherLocations *sharedMyWeathers = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyWeathers = [[self alloc] init];
    });
    return sharedMyWeathers;
}

- (id)init
{
    self = [super init];
    if ( self )
    {
        self.weathers = [[NSMutableArray alloc] init];
    }
    return self;
}

-(NSMutableArray*)getWeathers{
    return self.weathers;
}

-(void)addWeather:(Weather *)weather{
    return [self.weathers addObject:weather];
}

-(void)updateOptimunWeather{
    //For choose the optimus weather the algorithm is based in first priority in less cloudity else in less humidity.
    
    Weather * optimusWeather = [self.weathers firstObject];
    optimusWeather.optWeather = YES;
    if(self.weathers.count >1){
        for (int i=1; i<[self.weathers count]; i++) {
            Weather *weather = [self.weathers objectAtIndex:i];
            weather.optWeather = NO;
            if (([optimusWeather.cloudiness floatValue]> [weather.cloudiness floatValue])&&(![weather.condition isEqualToString:@"Rain"])) {
                weather.optWeather = YES;
                optimusWeather.optWeather = NO;
                optimusWeather = weather;
            }else if ([optimusWeather.cloudiness floatValue] == [weather.cloudiness floatValue]){
                if ([optimusWeather.humidity floatValue]> [weather.humidity floatValue]) {
                    weather.optWeather = YES;
                    optimusWeather.optWeather = NO;
                    optimusWeather = weather;
                }
            }
        }
    }
}
@end
