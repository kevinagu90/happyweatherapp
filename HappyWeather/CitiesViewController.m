//
//  CitiesViewController.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 7/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "CitiesViewController.h"
#import "WeatherLocations.h"
#import "WeatherViewCell.h"
#import "UIViewController+HWMethods.h"

NSString *const CELL = @"WeatherCell";
NSString *const CELL_NIB_NAME1  = @"WeatherViewCell";
float const CELL_HEIGHT_DEFAULT  = 123.0F;

@interface CitiesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CitiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCitiesNavigationBar];
    [self.tableView registerNib:[UINib nibWithNibName:CELL_NIB_NAME1 bundle:nil] forCellReuseIdentifier:CELL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-UITableView
 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {
 // Return the number of sections.
     return 1;
 }
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
 // Return the number of rows in the section.
 return [[[WeatherLocations sharedWeathers] getWeathers] count];;
 }
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
     WeatherViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL];
     Weather * weather = [[[WeatherLocations sharedWeathers] getWeathers] objectAtIndex:indexPath.row];
     [cell setWeather:weather];
     [cell configureCell];
     return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return CELL_HEIGHT_DEFAULT;
}

@end
