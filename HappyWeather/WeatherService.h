//
//  WeatherService.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 05/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "AFNetworking.h"
#import "GlobalVariables.h"
#import "Weather.h"

typedef void (^WeatherCompletion)(Weather *weather, NSError *error);
@interface WeatherService : NSObject
+(void)fetchWeatherByCoordinates:(CLLocation*)coordinates withCompletion:(WeatherCompletion)completion;
@end

