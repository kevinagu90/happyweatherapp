//
//  MapViewController.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 28/03/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "MapViewController.h"
#import "WeatherService.h"
#import "WeatherLocations.h"
#import "UIViewController+HWMethods.h"
#import "UIColor+UIColor_HWMethods.h"

@import GoogleMaps;

NSString *const markerIcon = @"markerIcon";
@interface MapViewController () <GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIView *guideView;
@property (nonatomic, retain) NSMutableArray<GMSMarker*> *markersArray;
@property (weak, nonatomic) IBOutlet UIView *gMapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain)Weather *currentWeather;
@end

@implementation MapViewController{
  //  Weather *currentWeather;
    GMSMapView *mapView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.closeButton setBackgroundColor:[UIColor blueHWColor]];
    [_guideView setBackgroundColor:[UIColor blueHWColor]];
    _currentWeather = [[[WeatherLocations sharedWeathers] getWeathers] firstObject];
    [self initMap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*!Initialize map view with current coordinates*/
-(void)initMap{
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[_currentWeather.latitud floatValue]
                                                            longitude:[_currentWeather.longitud floatValue]
                                                                 zoom:15];
    mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    mapView.delegate = self;
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([_currentWeather.latitud floatValue], [_currentWeather.longitud floatValue]);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:markerIcon];
    marker.map = mapView;
    _markersArray = [[NSMutableArray alloc] initWithObjects:marker, nil];
    [self.view addSubview:mapView];
}


#pragma mark-GMSMapView
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:markerIcon];
    marker.map = mapView;
    [_markersArray addObject:marker];
    CLLocation * location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        [WeatherService fetchWeatherByCoordinates:location withCompletion:^(Weather * weather, NSError *error) {
            [[WeatherLocations sharedWeathers] addWeather:weather];
            [[WeatherLocations sharedWeathers] updateOptimunWeather];
        }];
    });
}

-(void)setNavigationBar{
    UINavigationBar *navbar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    //do something like background color, title, etc you self
    [self.view addSubview:navbar];
}

//Action when close button is pressed
-(IBAction) closeView:(id) sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
