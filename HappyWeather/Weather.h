//
//  Weather.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 6/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface Weather : NSObject
@property (nonatomic, strong) NSNumber *latitud;
@property (nonatomic, strong) NSNumber *longitud;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSNumber *humidity;
@property (nonatomic, strong) NSNumber *temperature;
@property (nonatomic, strong) NSNumber *tempHigh;
@property (nonatomic, strong) NSNumber *tempLow;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSDate *sunrise;
@property (nonatomic, strong) NSDate *sunset;
@property (nonatomic, strong) NSString *conditionDescription;
@property (nonatomic, strong) NSString *condition;
@property (nonatomic, strong) NSNumber *cloudiness;
@property (nonatomic, strong) NSNumber *windBearing;
@property (nonatomic, strong) NSNumber *windSpeed;
@property (nonatomic, strong) NSString *icon;

/*!Property for set if this Weather is best for vacations*/
@property BOOL optWeather;

/*!Return image name for each weather*/
- (NSString *)imageName;

/*!Return cityName and countryName concatenated*/
-(NSString*)getLocationName;

-(NSString*)getCelsiusTemperature;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
