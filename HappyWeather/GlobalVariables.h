//
//  GlobalVariables.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 05/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVariables : NSObject
extern NSString *const API_URL_OSM;
extern NSString *const SUN_IMAGE;
extern NSString *const STORM_IMAGE;
extern NSString *const CLOUDY_IMAGE;
extern NSString *const RAIN_IMAGE;
extern NSString *const SNOW_IMAGE;
extern NSString *const DEFAULT_IMAGE;
@end
