//
//  WeatherViewController.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 5/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "WeatherViewController.h"
#import <LBBlurredImage/UIImageView+LBBlurredImage.h>
#import "UIViewController+HWMethods.h"
#import "WeatherService.h"
#import "WeatherLocations.h"
#import "UIColor+UIColor_HWMethods.h"


NSString *const backgroundImage = @"vacation";

@interface WeatherViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *blurImageView;
@property (weak, nonatomic) IBOutlet UIImageView *conditionImage;
@property (weak, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *conditionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (nonatomic,retain)Weather *currentWeather;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedViews;
@end

@implementation WeatherViewController{
    int currentPosition;
    UIImage *background;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPosition = 0;


    _currentWeather = [[[WeatherLocations sharedWeathers] getWeathers] firstObject];
    background = [UIImage imageNamed:backgroundImage];
    [self.blurImageView setImageToBlur:background blurRadius:10 completionBlock:nil];
    [self setViewElements];
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(setViewElements )userInfo:nil repeats:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self setTransparentNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*! Set all labels with data from service */
-(void)setViewElements{
    _currentWeather = [[[WeatherLocations sharedWeathers] getWeathers] objectAtIndex:currentPosition];
    UIColor *viewsColor;
    if(_currentWeather.optWeather ==YES){
        viewsColor = [UIColor orangeHWColor];
    }else{
        viewsColor = [UIColor whiteColor];
    }
    _conditionImage.image = [[UIImage imageNamed:[_currentWeather imageName]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_conditionImage setTintColor:viewsColor];
    _locationNameLabel.text = [_currentWeather getLocationName];
    _locationNameLabel.textColor = viewsColor;
    _conditionNameLabel.text = _currentWeather.condition;
    _conditionNameLabel.textColor = viewsColor;
    _tempLabel.text = [NSString stringWithFormat:@"%@º", [_currentWeather getCelsiusTemperature]];
    _tempLabel.textColor = viewsColor;
    if (currentPosition < [[[WeatherLocations sharedWeathers] getWeathers] count]-1) {
        currentPosition++;
    }else{
        currentPosition = 0;
    }
}




@end
