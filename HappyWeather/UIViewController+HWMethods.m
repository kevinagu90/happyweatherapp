//
//  UIViewController+HWMethods.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 5/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "UIViewController+HWMethods.h"
#import "UIColor+UIColor_HWMethods.h"


@implementation UIViewController (HWMethods)

-(void)setTransparentNavigationBar{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

-(void)setCitiesNavigationBar{
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.navigationBar.alpha = 0.7;
    self.navigationController.navigationBar.translucent = YES;
}

@end
