//
//  WeatherLocations.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 7/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weather.h"

@interface WeatherLocations : NSObject

-(id)init;

/*!Return a WeatherLocation globaly.*/
+ (instancetype)sharedWeathers;

/*!Return Array of Weathers.*/
-(NSMutableArray*)getWeathers;

/*!Add Weather to array*/
-(void)addWeather:(Weather*)weather;

/*!Update the optimun weather of array*/
-(void)updateOptimunWeather;
@end
