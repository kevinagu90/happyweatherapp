//
//  WeatherViewCell.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 7/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Weather.h"

@interface WeatherViewCell : UITableViewCell
@property (nonatomic, retain) Weather * weather;

/*!Set all UIView elements from weather*/
-(void)configureCell;
@end
