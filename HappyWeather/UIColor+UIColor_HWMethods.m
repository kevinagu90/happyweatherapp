//
//  UIColor+UIColor_HWMethods.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 7/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "UIColor+UIColor_HWMethods.h"

@implementation UIColor (UIColor_HWMethods)

+(UIColor*)blueHWColor{
    return [UIColor colorWithRed:(CGFloat)(126.0 / 255.0) green:(CGFloat)(192.0 /255.0) blue:(CGFloat)(238.0 / 255.0) alpha:1.0];
}

+(UIColor*)orangeHWColor{
        return [UIColor colorWithRed:(CGFloat)(255.0 / 255.0) green:(CGFloat)(116.0 /255.0) blue:(CGFloat)(0.0 / 255.0) alpha:1.0];
}
@end
