//
//  UIColor+UIColor_HWMethods.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 7/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (UIColor_HWMethods)
/*!Custom Orange color.*/
+(UIColor*)blueHWColor;

/*!Custom Blue color.*/
+(UIColor*)orangeHWColor;


@end
