//
//  LaunchViewController.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 6/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "LaunchViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "WeatherService.h"
#import <JTMaterialSpinner/JTMaterialSpinner.h>
#import "WeatherLocations.h"
#import "UIColor+UIColor_HWMethods.h"

NSString *const segueName = @"toWeatherView";
@interface LaunchViewController () <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet JTMaterialSpinner *spinnerView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (nonatomic, retain)Weather *currentWeater;
@end

@implementation LaunchViewController{
   // Weather *currentWeater;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocation *medellinCoordinates;
    CLLocation *bquillaCoordinates;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    locationManager = [[CLLocationManager alloc] init];
    [self.navigationController.navigationBar setHidden:YES];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    self.view.backgroundColor = [UIColor blueHWColor];
    self.spinnerView.backgroundColor = [UIColor blueHWColor];
    _loadingLabel.textColor = [UIColor orangeHWColor];
    medellinCoordinates = [[CLLocation alloc] initWithLatitude:6.2686734 longitude:-75.666433];
    bquillaCoordinates = [[CLLocation alloc] initWithLatitude:10.9915713 longitude:-74.9790929];
    // Customize the line width
    _spinnerView.circleLayer.lineWidth = 2.0;
    
    // Change the color of the line
    _spinnerView.circleLayer.strokeColor = [UIColor orangeHWColor].CGColor;
    [_spinnerView beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    if(currentLocation==nil){
        currentLocation = [locations firstObject];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        dispatch_async(queue, ^{
            [WeatherService fetchWeatherByCoordinates:medellinCoordinates withCompletion:^(Weather * weather, NSError *error) {
                if(!error){
                    _currentWeater = weather;
                    [[WeatherLocations sharedWeathers] addWeather:_currentWeater];
            
                    [WeatherService fetchWeatherByCoordinates:bquillaCoordinates withCompletion:^(Weather * weather, NSError *error) {
                        if(!error){
                            _currentWeater = weather;
                            [[WeatherLocations sharedWeathers] addWeather:_currentWeater];
                            
                            [WeatherService fetchWeatherByCoordinates:currentLocation withCompletion:^(Weather * weather, NSError *error) {
                                if(!error){
                                    _currentWeater = weather;
                                    [[WeatherLocations sharedWeathers] addWeather:_currentWeater];
                                    [[WeatherLocations sharedWeathers] updateOptimunWeather];
                                    [_spinnerView endRefreshing];
                                    [self performSegueWithIdentifier:segueName sender:nil];
                                    
                                }
                            }];
                            
                        }
                    }];
                 
                    
                }
            }];
        });
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
}



@end
