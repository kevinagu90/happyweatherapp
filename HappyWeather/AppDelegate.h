//
//  AppDelegate.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 28/03/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

