//
//  main.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 28/03/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
