//
//  WeatherViewCell.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 7/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "WeatherViewCell.h"
#import "WeatherLocations.h"
@interface WeatherViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UIImageView *conditionImage;
@property (weak, nonatomic) IBOutlet UILabel *conditionNameLabel;
@property (weak, nonatomic) IBOutlet UIView *conditionBackgroundView;

@end

@implementation WeatherViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell{
    _cityNameLabel.text = [_weather getLocationName];
    _tempLabel.text = [NSString stringWithFormat:@"%@ºC", [_weather getCelsiusTemperature]];
    _conditionImage.image = [UIImage imageNamed:[_weather imageName]];
    _conditionNameLabel.text = _weather.condition;
    if(_weather.optWeather == NO){
        _conditionBackgroundView.backgroundColor = [UIColor grayColor];
    }
}
@end
