//
//  UIViewController+HWMethods.h
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 5/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HWMethods)
-(void)setTransparentNavigationBar;

/*! Custom Navigation Bar for CitiesViewController */
-(void)setCitiesNavigationBar;

@end
