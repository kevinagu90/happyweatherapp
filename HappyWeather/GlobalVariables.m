//
//  GlobalVariables.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 05/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "GlobalVariables.h"

@implementation GlobalVariables
NSString *const API_URL_OSM = @"http://api.openweathermap.org/data/2.5/weather?";
NSString *const SUN_IMAGE = @"sunIcon";
NSString *const STORM_IMAGE = @"stormIcon";
NSString *const CLOUDY_IMAGE = @"cloudyIcon";
NSString *const RAIN_IMAGE = @"rainIcon";
NSString *const SNOW_IMAGE = @"snowIcon";
NSString *const DEFAULT_IMAGE = @"defaultWeatherIcon";
@end
