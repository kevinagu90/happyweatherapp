//
//  WeatherService.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 05/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "WeatherService.h"


@implementation WeatherService
+(void)fetchWeatherByCoordinates:(CLLocation*)coordinates withCompletion:(WeatherCompletion)completion{
    
    NSString *parameters = [NSString stringWithFormat:@"lat=%f&lon=%f&APPID=%@", coordinates.coordinate.latitude, coordinates.coordinate.longitude, [[NSBundle mainBundle] objectForInfoDictionaryKey:@"OSMApyKey"]];
    NSString * url = [NSString stringWithFormat:@"%@%@", API_URL_OSM, parameters];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //Weather object is instanced to parse response object dictionary.
        Weather * weather = [[Weather alloc] initWithDictionary:responseObject];
        completion(weather, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}
@end
