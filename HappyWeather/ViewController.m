//
//  ViewController.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 28/03/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
