//
//  Weather.m
//  HappyWeather
//
//  Created by kevin Agudelo Betancourt on 6/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "Weather.h"
#import "GlobalVariables.h"

NSString *const cloudsCondition = @"Clouds";
NSString *const rainCondition = @"Rain";
NSString *const clearCondition = @"Clear";
@implementation Weather
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self.latitud = [dictionary objectForKey:@"coord"][@"lat"];
    self.longitud = [dictionary objectForKey:@"coord"][@"lon"];
    self.humidity = [dictionary objectForKey:@"main"][@"humidity"];
    self.temperature = [dictionary objectForKey:@"main"][@"temp"];
    self.tempHigh = [dictionary objectForKey:@"main"][@"temp_max"];
    self.tempLow = [dictionary objectForKey:@"main"][@"temp_min"];
    self.cityName = [dictionary objectForKey:@"name"];
    self.countryName = [dictionary objectForKey:@"sys"][@"country"];
    self.sunrise = [dictionary objectForKey:@"sys"][@"sunrise"];
    self.sunset = [dictionary objectForKey:@"sys"][@"sunset"];
    self.windSpeed = [dictionary objectForKey:@"wind"][@"speed"];
    self.windBearing = [dictionary objectForKey:@"wind"][@"deg"];
    self.conditionDescription = [[[dictionary objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"description"];
    self.condition = [[[dictionary objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"main"];
    self.cloudiness = [dictionary objectForKey:@"clouds"][@"all"];
    self.optWeather = NO;
    return self;
}

-(NSString*)imageName{
    
    if([self.condition isEqualToString:cloudsCondition]){
        return CLOUDY_IMAGE;
    }else if ([self.condition isEqualToString:rainCondition]){
        return RAIN_IMAGE;
    }else if ([self.condition isEqualToString:clearCondition]){
        return SUN_IMAGE;
    }
    else{
        return DEFAULT_IMAGE;
    }

    }

-(NSString*)getLocationName{
    return [NSString stringWithFormat:@"%@, %@", self.cityName, self.countryName];
}

-(NSString*)getCelsiusTemperature{
    NSNumber *ctemP =
    @([self.temperature floatValue]-273.15);
    int intTemp = [ctemP intValue];
    return [@(intTemp) stringValue];
}
@end
